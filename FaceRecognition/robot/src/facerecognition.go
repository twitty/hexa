package FaceRecognition

import (
	"bytes"
	"encoding/base64"
	"image/jpeg"
	"os"

	"mind/core/framework/skill"
	"mind/core/framework"
	"mind/core/framework/drivers/hexabody"
	"mind/core/framework/drivers/media"
	"mind/core/framework/log"

	"github.com/lazywei/go-opencv/opencv"
)

type FaceRecognition struct {
	skill.Base
	stop chan bool
	cascade *opencv.HaarCascade
}

func NewSkill() skill.Interface {
	return &FaceRecognition{
		stop: make(chan bool),
		cascade: opencv.LoadHaarClassifierCascade("assets/haarcascade_frontalface_alt.xml")
	}
}

func (d *FaceRecognition) sight() {
	for {
		select {
		case <-d.stop:
			return
		default:
			image := media.SnapshotRGBA()
			buf := new(bytes.Buffer)
			jpeg.Encode(buf, image, nil)
			str := base64.StdEncoding.EncodeToString(buf.Bytes())
			framework.SendString(str)
			cvimg := opencv.FromImageUnsafe(image)
			faces := d.cascade.DetectObjects(cvimg)
			hexabody.StandWithHeight(flaot64(len(faces)) * 50)
		}
	}
}

func (d *FaceRecognition) OnStart() {
	log.Info.Println("Started")
}

func (d *FaceRecognition) OnClose() {
	hexabody.Close()
}

func (d *FaceRecognition) OnConnect() {
	err := hexabody.Start()
	if err != nil {
		log.Error.Println("Hexabody start err:\n", err)
		return
	}
	if !media.Available() {
		log.Error.Println("Media driver not available")
		return
	}
	if err := media.Start(); err != nil {
		log.Error.Println("Media driver could not start")
	}
}

func (d *FaceRecognition) OnDisconnect() {
	os.Exit(0)
}

func (d *FaceRecognition) OnRecvString(data string) {
	log.Info.Println(data)
	switch data {
	case "start":
		go d.sight()
	case "stop":
		d.stop <- true
	}
}
