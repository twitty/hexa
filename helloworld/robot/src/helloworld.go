package helloworld

import (
	"math"
	"mind/core/framework/drivers/distance"
	"mind/core/framework/drivers/hexabody"
	"mind/core/framework/log"
	"mind/core/framework/skill"
	"time"
)

type helloworld struct {
	skill.Base
}

func NewSkill() skill.Interface {
	// Use this method to create a new skill.

	return &helloworld{}
}

func newDirection(driection float64, degrees float64) float64 {
	return math.Mod(direction+degrees, 360) * -1
}

func (d *helloworld) OnStart() {
	// Use this method to do something when this skill is starting.
	hexabody.Start()
	distance.Start()
	hexabody.Stand()
}

func (d *helloworld) OnClose() {
	// Use this method to do something when this skill is closing.
	hexabody.Close()
	distance.Close()
}

func (d *helloworld) OnConnect() {
	// Use this method to do something when the remote connected.
	hexabody.MoveHead(0, 0)
	hexabody.WalkContinuously(0, 0.5)
	for {
		dist, _ := distance.Value()
		log.Info.Println("Distance in millimeters: ", dist)
		if dist < 500 {
			hexabody.StopWalkingContinuously()
			hexabody.Relax()
			time.Sleep(2 * time.Second)
			d.direction = newDirection(d.direction, 180)
			hexabody.MoveHead(d.direction, 0)
			time.Sleep(time.Second)
			hexabody.WalkContinuously(0, 0.5)
			time.Sleep(time.Second)
		}
		time.Sleep(500 * time.Millisecond)
	}
}

func (d *helloworld) OnDisconnect() {
	// Use this method to do something when the remote disconnected.
	hexabody.StopWalkingContinuously()
	hexabody.Relax()
}

func (d *helloworld) OnRecvJSON(data []byte) {
	// Use this method to do something when skill receive json data from remote client.
}

func (d *helloworld) OnRecvString(data string) {
	// Use this method to do something when skill receive string from remote client.
}
